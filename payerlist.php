<?php

/*   $host        = "host = localhost";
   $port        = "port = 5432";
   $dbname      = "dbname = Payer2Payer_Hub";
   $credentials = "user = postgres password=postgres";

   $db = pg_connect( "$host $port $dbname $credentials"  );
   if(!$db) {
      echo "Error : Unable to open database\n";
   } else {
      echo "Opened database successfully\n";
   }
?>*/

$host = "p2phubserver.postgres.database.azure.com";
$port = "5432";
$dbname = "p2phub";
$user = "p2phub";
$password = "Admin@1234"; 
$connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password} ";
$dbconn = pg_connect($connection_string);

function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKEY: 111111111111111111111',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

//if(isset($_POST['submit'])&&!empty($_POST['submit'])){
    
    
    /*$sql ="select *from public.users where email = '".pg_escape_string($_POST['email'])."' and password ='".$hashpassword."'";
    $data = pg_query($dbconn,$sql); 
    $login_check = pg_num_rows($data);
    if($login_check > 0){ 
        
        echo "Login Successfully";    
    }else{
        
        echo "Invalid Details";
    }*/
	
	
	$make_call = callAPI('GET', 'http://localhost:7000/api/organization',false);
	$response = json_decode($make_call, true);
	//print_r($response["data"]);
	//$errorcode   = $response['code'];
	//$data     = $response['results'];
	$data     = $response["data"];
	if(count($data) > 0){ 
        
        //echo "PayerList Successfully";    
    }else{
        
        //echo "Invalid Details";
    }
//}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PHP PostgreSQL Registration & Login Example </title>
  <meta name="keywords" content="PHP,PostgreSQL,Insert,Login">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
 
<h2>Payer List</h2>
                                                                                      
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
        <th>City</th>
        <th>Country</th>
      </tr>
    </thead>
    <tbody>
	<?php
    for($i=0;$i<count($data);$i++)
    {
?>
      <tr>
        <td><?php echo $data[$i]["payer_id"] ?></td>
        <td><?php echo $data[$i]["name"] ?></td>
        <td><?php echo $data[$i]["email"] ?></td>
        <td><?php echo $data[$i]["phone"] ?></td>
        <td><?php echo $data[$i]["address1"] ?></td>
        <td><?php echo $data[$i]["zip_code"] ?></td>
      </tr>
	  <?php
    }
 
?>
    </tbody>
  </table>
  </div>
  </div>
</body>
</html>
<?php

/*   $host        = "host = localhost";
   $port        = "port = 5432";
   $dbname      = "dbname = Payer2Payer_Hub";
   $credentials = "user = postgres password=postgres";

   $db = pg_connect( "$host $port $dbname $credentials"  );
   if(!$db) {
      echo "Error : Unable to open database\n";
   } else {
      echo "Opened database successfully\n";
   }
?>*/

$host = "p2phubserver.postgres.database.azure.com";
$port = "5432";
$dbname = "p2phub";
$user = "p2phub";
$password = "Admin@1234"; 
$connection_string = "host={$host} port={$port} dbname={$dbname} user={$user} password={$password} ";
$dbconn = pg_connect($connection_string);

$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
// echo 'Full URL '.$fullurl;
session_start();
$url = $fullurl;
     
// Use parse_url() function to parse the URL
// and return an associative array which
// contains its various components
$url_components = parse_url($url);
 
// Use parse_str() function to parse the
// string passed via URL
parse_str($url_components['query'], $params);
     
// Display result
//  echo ' Status id-- '.$params['id'];
 $_SESSION["statusid"] = $params['id'];
 
function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKEY: 111111111111111111111',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

if(isset($_POST['submit'])&&!empty($_POST['submit'])){
    
    $hashpassword = md5($_POST['pwd']);
    /*$sql ="select *from public.users where email = '".pg_escape_string($_POST['email'])."' and password ='".$hashpassword."'";
    $data = pg_query($dbconn,$sql); 
    $login_check = pg_num_rows($data);
    if($login_check > 0){ 
        
        echo "Login Successfully";    
    }else{
        
        echo "Invalid Details";
    }*/
	
	$email=$_POST['email'];
	$password=$_POST['pwd'];
	$data_array =  array(
		  "email"        => $email,
		  "password"         => $password,
	);
	$make_call = callAPI('POST', 'http://13.92.80.150:7000/api/user/login', json_encode($data_array));
	$response = json_decode($make_call, true);
	//print_r($response);
	$errorcode   = $response['code'];
	//$data     = $response['results'];
	$data     = $response;
	if($errorcode==200){ 
        
        echo "Login Successfully";   
		header('Location: contractform.php');		
    }else{
        
        echo "Invalid Details";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payer Developer Portal </title>
  <meta name="keywords" content="PHP,PostgreSQL,Insert,Login">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="">
  
   <div class="panel panel-info">
      <div class="panel-heading">Sign in with HealthChain</div>
      <div class="panel-body">
         <form method="post">
         
               <div class="form-group" style="text-align:center">
                  <img src="logo.png" style="max-width: 10%;">
               </div> 
               <div class="form-group" style="text-align:center">
                 <p style="font-size:18px"> Sign in</p> to continue to BCBSRIFL
               </div>    
               <div class="form-group">
                  <label for="email">Email:</label>
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
               </div>
               
               
               <div class="form-group">
                  <label for="pwd">Password:</label>
                  <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
               </div>
               <P> To continue, HealthChain will share your name, email and profile picture with BCBSRIFL. Before using this app, you can review BCBSRIFL <a href="">Privacy Policy</a> and <a href="">Terms of Service</a> </P>
               
              <div style="text-"> <input type="submit" name="submit" class="btn btn-primary" value="SIGN IN"></div>
                  
               <!-- <input type="submit" name="submit" class="btn btn-primary btn-block" value="LOGIN WITH HEALTHCHAIN"> -->
         </form>
      </div>
    </div>

  <!-- <h2 style="text-align:center">Login with HealthChain </h2>
  <form method="post" style="BORDER: 2px solid #642d2d; margin: 20px; padding: 30px; border-radius: 12px;">
  
  
     
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    
     
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
    </div>
     
    <input type="submit" name="submit" class="btn btn-primary" value="Submit">
  </form> -->

  </div>
</body>
</html>